# A basic python replication framework prototype
[![pipeline status](https://gitlab.com/slumber/replication/badges/master/pipeline.svg)](https://gitlab.com/slumber/replication/commits/master)
> A simple client/server python objects replication framework 

## Dependencies

| Dependencies | Version | Needed |
|--------------|:-------:|-------:|
| ZeroMQ       | latest  |    yes |
| msgpack     | latest  |    yes |

## Contributing

1. Fork it (<https://gitlab.com/yourname/yourproject/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request