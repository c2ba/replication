import logging
import threading
import time

import zmq

from .data import ReplicatedCommand, RepDeleteCommand, ReplicatedDatablock
from .graph import ReplicationGraph
from .constants import MODIFIED, STATE_INITIAL, STATE_ACTIVE, UP, RP_COMMON, RP_STRICT

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

class Observer(threading.Thread):
    def __init__(
            self,
            port=None,
            address=None,
            store_reference=None,
            watched_type=None,
            timeout=2,
            automatic=False,
            session_instance=None,
            context=zmq.Context.instance(),
            lock=None):

        # Threading
        threading.Thread.__init__(self)
        self.name = "{}_watchdog".format(str(watched_type.__name__))
        self.daemon = False

        self._timeout = timeout
        self._watched_type = watched_type
        self._exit_event = threading.Event()
        self._repo = store_reference
        self._automatic = automatic
        self._state = STATE_INITIAL
        self._session = session_instance
        self._lock = lock

    def run(self):
        self._state = STATE_ACTIVE
        logger.debug("{} running".format(self.name))
        local_user = self._session._id

        while not self._exit_event.wait(self._timeout):
           
            if self._session._right_strategy == RP_COMMON:
                if self._watched_type.__name__ in ['BlCollection','BlScene']:
                    keys_to_check = [k for k in self._session.list(filter=self._watched_type)
                                    if self._session.get(uuid=k).owner in [RP_COMMON, local_user]]
                else:
                    keys_to_check = self._session.list(
                        filter_owner=local_user, filter=self._watched_type)
            else:
                keys_to_check = self._session.list(
                    filter_owner=local_user, filter=self._watched_type)


            for key in keys_to_check:
                node = self._repo[key]

                if node.state == UP:
                    try:
                        if node.diff():
                            logger.info("{} > Diff found on {} block \n ".format(self._session._id,node.str_type))
                            if self._automatic:
                                self._session.commit(node.uuid)
                                self._session.push(node.uuid)
                    except ReferenceError:
                        node.resolve()

                        if not node.is_valid():
                            self._session.remove(node.uuid)
                    except Exception:
                        continue
                        

        self._exit_event.clear()

    def stop(self):
        if self._state != STATE_ACTIVE:
            return

        self._exit_event.set()
        self._state = 0
        self.join()
