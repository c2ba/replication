class NonAuthorizedOperationError(Exception):
   """Base class for other exceptions"""
   pass
