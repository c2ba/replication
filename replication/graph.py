import collections


class ReplicationGraph(collections.MutableMapping):
    """
    Structure to hold replicated data relation graph
    """

    def __init__(self, *args, **kwargs):
        self.store = dict()
        self.update(dict(*args, **kwargs))  # use the free update to set keys

    def __getitem__(self, key):
        return self.store[key]

    def __setitem__(self, key, value):
        self.store[key] = value

    def __delitem__(self, key):
        del self.store[key]

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def __repr__(self):
        str = "\n"
        for key, item in self.store.items():
            str += repr(item)
        return str

    def list(self, filter_state=None, filter_type=None):
        if filter_state:
            return [key for key, item in self.store.items()
                    if item.state == filter_state]
        if filter_type:
            return [key for key, item in self.store.items()
                    if isinstance(item, filter_type)]
        return [key for key, item in self.store.items()]

    def get_dependencies(self, node, deps=[]):
        node_ref = self.get(node)
        
        if node_ref and node_ref.dependencies:
            for child in node_ref.dependencies:
                if child not in deps:
                    deps.append(child)
                self.get_dependencies(child, deps)
