import json
import logging
import jsondiff
import pickle
from enum import Enum
from uuid import uuid4

import zmq
import msgpack

from .constants import (
    ADDED, COMMITED,  PUSHED,
    FETCHED, UP, ERROR, MODIFIED)

logger = logging.getLogger(__name__)


class ReplicatedDataFactory(object):
    """
    Manage the data types implementations.

    """

    def __init__(self):
        self.supported_types = []

    def register_type(
            self,
            source_type,
            implementation,
            timer=0,
            automatic=False):
        """
        Register a new replicated datatype implementation
        """
        self.supported_types.append(
            (source_type, implementation, timer, automatic))

    def match_type_by_instance(self, data):
        """
        Find corresponding type to the given datablock
        """
        for stypes, implementation, time, auto in self.supported_types:
            if isinstance(data, stypes):
                return implementation
        logger.error("{} not supported for replication".format(data))

    def match_type_by_name(self, type_name):
        for stypes, implementation, time, auto in self.supported_types:
            if type_name == implementation.__name__:
                return implementation
        logger.error("{} not supported for replication".format(type_name))

    def get_implementation_from_object(self, data):
        return self.match_type_by_instance(data)

    def get_implementation_from_net(self, type_name):
        """
        Reconstruct a new replicated value from serialized data
        """
        return self.match_type_by_name(type_name)


class ReplicatedDatablock(object):
    """
    Datablock definition that handle object replication logic.
    PUSH: send the object over the wire
    STORE: register the object on the given replication graph
    LOAD: apply loaded changes by reference on the local copy
    DUMP: get local changes

    """
    uuid = None     # uuid used as key      (string)
    pointer = None  # dcc data ref          (DCC type)
    data = None   # raw data              (json)
    str_type = None  # data type name        (string)
    dependencies = [None]   # dependencies array    (string)
    owner = None    # Data owner            (string)
    state = None    # Data state            (RepState)

    def __init__(
            self,
            owner=None,
            pointer=None,
            uuid=None,
            data=None,
            bytes=None,
            dependencies=[]):
        assert(owner)

        self.uuid = uuid if uuid else str(uuid4())
        self.owner = owner

        if pointer:
            self.pointer = pointer
            self.state = ADDED
        elif data:
            self.data = data
            self.state = COMMITED
        elif bytes:
            self.data = self.deserialize(bytes)
            self.state = FETCHED

        self.str_type = type(self).__name__
        self.dependencies = dependencies

    def commit(self):
        assert(self.pointer and
               self.state in [MODIFIED, ADDED, UP])

        try:
            self.data = self.dump(pointer=self.pointer)
            self.state = COMMITED
        except Exception as e:
            logger.error("{} commit error: {}".format(self.str_type, e))
            self.state = ERROR

    def push(self, socket):
        """
        Here send data over the wire:
            - serialize the data
            - send them as a multipart frame thought the given socket
        """

        if self.state in [COMMITED, UP]:
            data = self.serialize(self.data)
            owner = self.owner.encode()
            key = self.uuid.encode()
            type = self.str_type.encode()
            dependencies = msgpack.packb(self.dependencies, use_bin_type=True)

            socket.send_multipart(
                [socket.IDENTITY, key, owner, type, data, dependencies])

            self.state = UP
        else:
            logger.error("Attempting to push uncomited node {}".format(self.str_type))

    @classmethod
    def fetch(cls, socket, factory=None):
        """
        Here we reeceive data from the wire:
            - read data from the socket
            - reconstruct an instance
        """

        identity, uuid, owner, str_type, data, dependencies = socket.recv_multipart(
            0)

        str_type = str_type.decode()
        owner = owner.decode()
        uuid = uuid.decode()
        dependencies = msgpack.unpackb(dependencies, raw=False)
        dependencies = dependencies if dependencies else None

        implementation = factory.get_implementation_from_net(str_type)

        instance = implementation(
            owner=owner, uuid=uuid, dependencies=dependencies, bytes=data)
        instance.sender = identity
        return instance

    def apply(self):
        """Apply stored data
        """
        # UP in case we want to reset our pointer data
        assert(self.state in [FETCHED, UP])
        logger.info("Applying {} ".format(self.str_type))

        if self.pointer is None:
            self.set_pointer()
        try:
            self.load(data=self.data, target=self.pointer)
            self.state = UP
        except Exception as e:
            logger.error("Load {} failed:\n {}".format(self.uuid, e))

    def set_pointer(self):
        self.resolve()

        if self.pointer is None:
            self.pointer = self.construct(self.data)
    
    def is_valid(self):
        raise NotImplementedError

    def on_post_apply(self):
        pass

    def construct(self, data=None):
        """Construct a new instance of the target object,
        assign our pointer to this instance
        """
        raise NotImplementedError

    def store(self, dict):
        """
        I want to store my replicated data. Persistent means into the disk
        If uuid is none we delete the key from the volume
        """
        if self.uuid is not None:
            if self.uuid in dict:
                dict[self.uuid].data = self.data
                dict[self.uuid].state = self.state
                dict[self.uuid].dependencies = self.dependencies
            else:
                dict[self.uuid] = self


            return self.uuid

    def deserialize(self, data):
        """
        BUFFER -> JSON
        """
        return msgpack.unpackb(data, raw=False, use_list=True )

    def serialize(self, data):
        """
        JSON -> BUFFER
        """
        return msgpack.packb(data, use_bin_type=True)

    def dump(self, pointer=None):
        """
        DCC -> JSON
        """
        assert(pointer)

        return json.dumps(pointer)

    def load(self, data=None, target=None):
        """
        JSON -> DCC
        """
        raise NotImplementedError

    def resolve(self):
        """
        I want to resolve my orphan data to an existing one
        = Assing my pointer

        """
        return None

    def diff(self):
        """Compare stored data to the actual one.

        return True if the versions doesn't match
        """
        diff = jsondiff.diff(
            self.data, self.dump(pointer=self.pointer), marshal=True, syntax='explicit')

        if diff:
            self.state = MODIFIED
            return True
        return False

    def resolve_dependencies(self):
        """Return a list of dependencies
        """
        return []

    def add_dependency(self, dependency):
        if not self.dependencies:
            self.dependencies = []
        if dependency not in self.dependencies:
            self.dependencies.append(dependency)

    def __repr__(self):
        return "\n {uuid} - owner: {owner} - type: {type}".format(
            uuid=self.uuid,
            owner=self.owner,
            type=self.str_type
        )


class ReplicatedCommandFactory(object):
    """
    Manage the data types implementations.

    """

    def __init__(self):
        self.supported_types = []

        self.register_type(RepDeleteCommand, RepDeleteCommand)
        self.register_type(RepRightCommand, RepRightCommand)
        self.register_type(RepConfigCommand, RepConfigCommand)
        self.register_type(RepSnapshotCommand, RepSnapshotCommand)
        self.register_type(RepAuthCommand, RepAuthCommand)
        self.register_type(RepDisconnectCommand, RepDisconnectCommand)
        self.register_type(RepUpdateClientsState, RepUpdateClientsState)
        self.register_type(RepUpdateUserMetadata, RepUpdateUserMetadata)

    def register_type(
            self,
            source_type,
            implementation):
        """
        Register a new replicated datatype implementation
        """
        self.supported_types.append(
            (source_type, implementation))

    def match_type_by_name(self, type_name):
        for stypes, implementation in self.supported_types:
            if type_name == implementation.__name__:
                return implementation
        logger.error("{} not supported for replication".format(type_name))

    def get_implementation_from_object(self, data):
        return self.match_type_by_instance(data)

    def get_implementation_from_net(self, type_name):
        """
        Reconstruct a new replicated value from serialized data
        """
        return self.match_type_by_name(type_name)


class ServerReplicatedDatablock(ReplicatedDatablock):
    def __init__(
            self,
            owner=None,
            uuid=None,
            data=None,
            bytes=None,
            str_type=None,
            dependencies=[],
            sender=None):
        assert(owner)

        self.uuid = uuid if uuid else str(uuid4())
        self.owner = owner
        self.data = msgpack.unpackb(bytes, raw=False,use_list=True)
        self.str_type = str_type
        self.dependencies = dependencies
        self.sender = sender

    @classmethod
    def fetch(cls, socket, factory=None):
        """
        Here we reeceive data from the wire:
            - read data from the socket
            - reconstruct an instance
        """

        identity, uuid, owner, str_type, data, dependencies = socket.recv_multipart(
            0)

        str_type = str_type.decode()
        owner = owner.decode()
        uuid = uuid.decode()
        dependencies = msgpack.unpackb(dependencies, raw=False)
        dependencies = dependencies if dependencies else None

        return ServerReplicatedDatablock(
            owner=owner,
            uuid=uuid,
            dependencies=dependencies,
            bytes=data,
            str_type=str_type,
            sender=identity)

    def push(self, socket, full=False):
        """
        Here send data over the wire:
            - serialize the data
            - send them as a multipart frame thought the given socket
        """

        data = msgpack.packb(self.data,use_bin_type=True)
        owner = self.owner.encode()
        key = self.uuid.encode()
        type = self.str_type.encode()
        dependencies = msgpack.packb(self.dependencies, use_bin_type=True)

        socket.send_multipart(
            [socket.IDENTITY, key, owner, type, data, dependencies])

    def store(self, dict, persistent=False):
        """
        I want to store my replicated data. Persistent means into the disk
        If uuid is none we delete the key from the volume
        """
        if self.uuid is not None:
            if self.uuid in dict:
                dict[self.uuid].data = self.data
                dict[self.uuid].state = self.state
                dict[self.uuid].dependencies = self.dependencies
            else:
                dict[self.uuid] = self

            return self.uuid


class ReplicatedCommand():
    def __init__(
            self,
            owner=None,
            data=None):
        assert(owner)

        self.owner = owner
        self.data = data
        self.str_type = type(self).__name__

    def push(self, socket):
        """
        Here send data over the wire:
            - serialize the data
            - send them as a multipart frame thought the given socket
        """
        data = msgpack.packb(self.data, use_bin_type=True)
        owner = self.owner.encode()
        type = self.str_type.encode()

        socket.send_multipart([owner, type, data])

    @classmethod
    def fetch(cls, socket, factory=None):
        """
        Here we reeceive data from the wire:
            - read data from the socket
            - reconstruct an instance
        """

        owner, str_type, data = socket.recv_multipart(0)

        str_type = str_type.decode()
        owner = owner.decode()
        data = msgpack.unpackb(data, raw=False,use_list=True)

        implementation = factory.get_implementation_from_net(str_type)

        instance = implementation(owner=owner, data=data)
        return instance

    @classmethod
    def server_fetch(cls, socket, factory=None):
        """
        Here we reeceive data from the wire:
            - read data from the socket
            - reconstruct an instance
        """

        identity, owner, str_type, data = socket.recv_multipart(0)

        str_type = str_type.decode()
        owner = owner.decode()
        data = msgpack.unpackb(data, raw=False,use_list=True)

        implementation = factory.get_implementation_from_net(str_type)

        instance = implementation(owner=owner, data=data)
        instance.sender = identity
        return instance

    def execute(self, graph):
        raise NotImplementedError


class RepDeleteCommand(ReplicatedCommand):
    def execute(self, graph):
        assert(self.data)

        if graph and self.data in graph.keys():
            # Clean all reference to this node
            for key, value in graph.items():
                if value.dependencies and self.data in value.dependencies:
                    value.dependencies.remove(self.data)
            # Remove the node itself
            del graph[self.data]


class RepRightCommand(ReplicatedCommand):
    def execute(self, graph):
        assert(self.data)

        if graph and self.data['uuid'] in graph.keys():
            graph[self.data['uuid']].owner = self.data['owner']


class RepConfigCommand(ReplicatedCommand):
    pass


class RepSnapshotCommand(ReplicatedCommand):
    pass


class RepAuthCommand(ReplicatedCommand):
    pass


class RepDisconnectCommand(ReplicatedCommand):
    pass


class RepUpdateClientsState(ReplicatedCommand):
    pass


class RepUpdateUserMetadata(ReplicatedCommand):
    pass