import logging
import queue
import time
import threading

from .constants import (ADDED, COMMITED, FETCHED,
                                   STATE_ACTIVE, STATE_INITIAL,
                                   STATE_SYNCING, UP, MODIFIED,
                                   RP_COMMON, RP_STRICT)
from .data import (ReplicatedCommand, RepDeleteCommand,
                              ReplicatedDatablock, RepRightCommand,
                              RepDisconnectCommand, RepUpdateClientsState,
                              RepUpdateUserMetadata)
from .graph import ReplicationGraph
from .network import (ClientNetService,
                                 ServerNetService,
                                 ClientEventHandler,
                                 ServerTTL)
from .observer import Observer
from .exception import NonAuthorizedOperationError

logger = logging.getLogger(__name__)

class Session(object):
    def __init__(self, factory=None):
        assert(factory)

        self._graph = ReplicationGraph()
       
        self._item_to_push = queue.Queue()
        self._net_server = None
        self._factory = factory
        self._id = None
        self._ttl_port = None
        self._observers = []
        self._right_strategy = RP_STRICT
        self._config = dict()
        self._online_users = {}

        self._event_handler = ClientEventHandler()
        self._event_handler.bind_on_config_update(self._on_config_update)
        self._event_handler.bind_on_server_lost(self._on_server_lost)
        self._event_handler.bind_on_connection(self._on_connected)
        self._event_handler.bind_on_disconnect_confirm(self.quit)
        self._event_handler.bind_on_clients_update(self._update_online_users)

        self._lock = threading.Event()

        self._net_client = ClientNetService(
            store_reference=self._graph,
            factory=factory,
            event_handler=self._event_handler,
            items_to_push=self._item_to_push,
            lock=self._lock)
        self._net_ttl = None

    def _nodes_in_state(self, state=None):
        added_items = self._graph.list(filter_state=state)
        stack = []

        for item in added_items:
            item_deps = []
            self._graph.get_dependencies(item, deps=item_deps)

            for deps in item_deps:
                if deps not in stack and self._graph[deps].state == state:
                    stack.append(deps)

            if item not in stack and self._graph[item].state == state:
                stack.append(item)

        return stack

    def _node_deps(self, node=None):
        assert(node)

        deps = []
        self._graph.get_dependencies(node, deps=deps)

        return list(reversed(deps))
    
    def _assert_modification_rights(self, node=None):
        if self._right_strategy == RP_STRICT and self._id != self._graph[node].owner:
            raise NonAuthorizedOperationError(
                "Not authorized to delete the node")

    def _check_dependencies(self, node_uuid):
        node = self._graph[node_uuid]

        assert(node)
        if not node.pointer:
            return

        if node.dependencies:
            node.dependencies.clear()
        dependencies = node.resolve_dependencies()

        for dep in dependencies:
            if dep:
                dep_node = self.get(reference=dep)
                if not dep_node:
                    dep_node_uuid = self.add(dep,owner=node.owner)
                else:
                    dep_node_uuid = dep_node.uuid

                node.add_dependency(dep_node_uuid)
    
    def _load_config(self, config):
        self._right_strategy = config["right_strategy"]

    def _on_config_update(self, config):
        self._load_config(config)
    
    def _on_server_lost(self):
        self.quit()

    def _on_connected(self):
        # 1. GRAPH CONSTRUCTION
        # TODO: Refactor

        roots = []
        childs =  []
        nodes = self.list()
        
        for node in nodes:
            ref = self.get(node)
            
            if ref.dependencies:
                for dep in ref.dependencies:
                    if dep not in childs:
                        childs.append(dep)
        
        roots = [n for n in nodes if n not in childs]

        for root in roots:
            root_node = self.get(root)

            if root_node.state == FETCHED:
                nodes_to_constuct = self._node_deps(root)

                for node in nodes_to_constuct:
                    node_ref = self.get(node)
                    node_ref.set_pointer()
                
                root_node.set_pointer()
        # 2. APPLY 
        # Just a temporary fix ! Will be remove with event driven update 
        self.apply_all()

        # 3. OBSERVERS STARTING
        for observer in self._observers:
            observer.start()

    def _update_online_users(self, status_update):
        self._online_users = status_update

    def connect(self,
                id="Default",
                address="127.0.0.1",
                port=5560):
        """Connect to a session

        :param id: user name
        :type id: string
        :param address: host ip address
        :type address: string
        :param port: host port
        :type port: int
        """
        self._id = id
        self._address = address
        self._net_client.connect(
            id=id, 
            address=address,
            port=port)

        for stype, implementation, timer, auto in self._factory.supported_types:
            if timer > 0:
                self._observers.append(Observer(address=address,
                                                port=port,
                                                store_reference=self._graph,
                                                watched_type=implementation,
                                                timeout=timer,
                                                automatic=auto,
                                                session_instance=self,
                                                lock=self._lock))

    def host(self,
             id="Default",
             address="127.0.0.1",
             port=5560,
             right_strategy=RP_STRICT):
        """Host a session

        :param id: user name
        :type id: string
        :param address: host ip address
        :type address: strings
        :param port: host port
        :type port: int
        """
        # Create a server and serve
        self._net_server = Server(
            config={
                'right_strategy': right_strategy,
            })
        try:
            self._net_server.serve(port=port)

            time.sleep(0.2)  # TODO: event driven wait
            assert(self._net_server.state == STATE_ACTIVE)

            # Connect
            self.connect(id=id, address=address, port=port)
            time.sleep(.1)

        except Exception as e:
            self.disconnect()
            raise ConnectionAbortedError(e)

    def disconnect(self):
        """Disconnect from session
        """

        if self.state == STATE_ACTIVE:        
            if self._net_server:
                self.quit()
            else:
                deco_request = RepDisconnectCommand(
                    owner='client'
                )
                
                self._item_to_push.put(deco_request)
        else:
            self.quit()

    def quit(self):
        logger.info("Quiting sesssion")
        self._net_client.stop()

        if self._net_server:
            self._net_server.stop()

        # Stop observers
        for observer in self._observers:
            observer.stop()

        self._graph.clear()
        self._online_users.clear()

    @property
    def state(self):
        """Get active session state
        0: STATE_INITIAL
        1: STATE_SYNCING
        2: STATE_ACTIVE

        :return: session state
        """
        return self._net_client._state

    @property
    def online_users(self):
        return self._online_users

    def add(self, object, owner=None, dependencies=None):
        """Register a python object for replication

        :param objet: Any registered object
        :type object: Any registered object type in the given factory
        :param dependencies: Object dependencies uuid
        :type dependencies: Array of string
        """
        assert(object)

        # Retrieve corresponding implementation and init a new instance
        implementation = self._factory.get_implementation_from_object(
            object)

        if implementation:
            default_owner = RP_COMMON if self._right_strategy == RP_COMMON else self._id
            
            new_owner = owner if owner else default_owner
            new_node = implementation(
                owner=new_owner, pointer=object, dependencies=dependencies)

            if new_node:
                dependencies = new_node.resolve_dependencies()

                for dependance in dependencies:
                    dep_ref = self.get(reference=dependance)
                    if dep_ref:
                        new_node.add_dependency(dep_ref.uuid)
                    else:
                        if dependance:
                            new_child_node = self.add(object=dependance)
                            new_node.add_dependency(new_child_node)

                logger.debug("Registering {} on {}".format(
                    object, new_node.uuid))
                new_node.store(self._graph)

                return new_node.uuid
        else:
            raise TypeError("Type not implemented")

    def remove(self, uuid, remove_dependencies=True):
        """
        Unregister for replication the given object.

        :param uuid: node uuidñ
        :type uuid: string
        :param remove_dependencies: remove all dependencies 
        :type remove_dependencies: bool (default: True)
        :raise NonAuthorizedOperationError:
        :raise KeyError: 
        """
        self._assert_modification_rights(uuid)

        if uuid in self._graph.keys():
            nodes_to_delete = []

            if remove_dependencies:
                nodes_to_delete.extend(self._node_deps(node=uuid))

            nodes_to_delete.append(uuid)

            for node in nodes_to_delete:
                delete_command = RepDeleteCommand(
                    owner='client', data=node)
                # remove the key from our store
                delete_command.execute(self._graph)
                self._item_to_push.put(delete_command)

        else:
            raise KeyError("Cannot unregister key")

    def commit(self, uuid):
        """Commit the given node

        :param uuid: node uuid
        :type uuid: string
        """
        assert(self.exist(uuid))

        if self._graph[uuid].state == COMMITED:
            return 

        self._check_dependencies(uuid)

        for node in self._node_deps(node=uuid):
            if self._graph[node].state in [ADDED, MODIFIED]:
                self.commit(node)

        self._graph[uuid].commit()

    def push(self, uuid):
        """Replicate a given node to all users. Send all node in `COMMITED` by default. 

        :param uuid: node key to push
        :type uuid: string
        """
        # TODO: better depencies handling
        if uuid:    
            self._assert_modification_rights(uuid)

            node = self._graph[uuid]
            if node.state == COMMITED:
                for dep in self._node_deps(node=uuid):
                    dep_node = self._graph[dep]
                    if dep_node.state == COMMITED:
                       self.push(dep_node.uuid)

                self._item_to_push.put(node)
    
    def push_all(self):
            for node in self._nodes_in_state(state=COMMITED):
                self._item_to_push.put(self.get(uuid=node))

    def apply(self, uuid=None, force=False):
        """Apply incoming modifications to local object(s) instance

        :param uuid: node key to push
        :type uuid: string
        """
        assert(self._graph[uuid].state in [FETCHED, UP])

        deps = self._node_deps(node=uuid)

        for dep in deps:
            node = self.get(uuid=dep)
            
            # Check dependencies states, abort if needed 
            if node and node.state == FETCHED:
                if force:
                    self.apply(uuid=node.uuid, force=force)
                else:
                    return

        self.get(uuid=uuid).apply()

    def apply_all(self):
        nodes = self._nodes_in_state(state=FETCHED)

        for n in nodes:
            self.apply(n, force=True)

    def change_owner(self, uuid, new_owner, recursive=True):
        """Change a node owner

        :param uuid: node key
        :type uuid: string
        :param new_owner: new owner id
        :type new_owner: string
        """
        assert(uuid and new_owner)
        
        # and self._graph[uuid].owner != self._id
        if uuid in self._graph.keys() and self._graph[uuid].owner in [RP_COMMON, self._id]:
            if recursive:
                for node in self._node_deps(node=uuid):
                    self.change_owner(uuid=node, new_owner=new_owner, recursive=recursive)

            # Setup the right override command
            right_command = RepRightCommand(
                owner=self._id,
                data={
                        'uuid': uuid,
                        'owner': new_owner}
            )

            # Apply localy
            right_command.execute(self._graph)

            # Dispatch on clients
            self._item_to_push.put(right_command)

    def get(self, uuid=None, reference=None):
        """Get a node ReplicatedDatablock instance

        :param uuid: node uuid
        :type uuid: string
        :return: ReplicatedDatablock
        """
        target = None

        if uuid:
            target = self._graph.get(uuid)
        if reference:
            for k, v in self._graph.items():
                if reference == v.pointer:
                    target = v
                    break

        return target

    def get_config(self):
        return self._net_client._config

    def update_user_metadata(self, dikt):
        """Update user metadata

        Update local client informations to others (ex: localisation)

        :param json:
        :type dict: 
        """
        assert(dikt)
        
        state_update_request = RepUpdateUserMetadata(
            owner=self._id,
            data=dikt
        )

        self._item_to_push.put(state_update_request)

    def exist(self, uuid=None, reference=None):
        """Check for a node existence

        :param uuid: node uuid
        :type uuid: string
        :return: bool
        """
        if uuid:
            return uuid in self._graph.keys()
        if reference:
            for k, v in self._graph.items():
                if reference == v.pointer:
                    return True

        return False

    def list(self, filter=None, filter_owner=None):
        """List all graph nodes keys 
        :param filter: node type
        :type filter: ReplicatedDatablock class (or child class)
        """
        base_list = self._graph.list(filter_type=filter)
        if filter_owner:
            return [key for key in base_list
                    if self._graph[key].owner == filter_owner]
        else:
            return base_list


class Server():
    def __init__(self, config=None):        
        self._net = ServerNetService(config=config)

    def serve(self, port=5560):
        self._net.listen(port=port)

    @property
    def state(self):
        return self._net._state

    def stop(self):
        self._net.stop()

