import logging
import threading
import time
import zmq
import msgpack
import copy
import uuid
import string
import random

from .data import (ReplicatedCommand,
                              RepDeleteCommand,
                              RepConfigCommand,
                              RepAuthCommand,
                              RepSnapshotCommand,
                              ReplicatedDatablock,
                              RepRightCommand,
                              ServerReplicatedDatablock,
                              ReplicatedCommandFactory,
                              RepDisconnectCommand,
                              RepUpdateClientsState,
                              RepUpdateUserMetadata)
from .graph import ReplicationGraph
from .constants import (STATE_ACTIVE, STATE_INITIAL,
                                   STATE_SYNCING, STATE_CONFIG,
                                   STATE_AUTH, COMMITED, RP_COMMON,
                                   RP_STRICT, RP_COMMON)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def current_milli_time():
    return int(round(time.time() * 1000))

def random_string_digits(stringLength=6):
    """Generate a random string of letters and digits """
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join(random.choices(lettersAndDigits, k=stringLength))

# TODO: replace ClientEventHandler by a threading event system
class ClientEventHandler():
    def __init__(self):
        self.on_config_update_callback = None
        self.on_server_lost_callback = None
        self.on_connection_callback = None
        self.on_disconnect_confirm_callback = None
        self.on_clients_update_callback = None

    def bind_on_config_update(self, f):
        self.on_config_update_callback = f

    def bind_on_server_lost(self, f):
        self.on_server_lost_callback = f

    def bind_on_connection(self, f):
        self.on_connection_callback = f
    
    def bind_on_disconnect_confirm(self, f):
        self.on_disconnect_confirm_callback = f

    def bind_on_clients_update(self, f):
        self.on_clients_update_callback = f

    def on_config_update(self, config):
        if self.on_config_update_callback:
            self.on_config_update_callback(config)

    def on_clients_update(self, status_update):
        if self.on_clients_update_callback:
            self.on_clients_update_callback(status_update)

    def on_server_lost(self):
        if self.on_server_lost_callback:
            self.on_server_lost_callback()

    def on_connection(self):
        if self.on_connection_callback:
            self.on_connection_callback()
    
    def on_disconnect_confirm(self):
        if self.on_disconnect_confirm_callback:
            self.on_disconnect_confirm_callback()


class ClientNetService(threading.Thread):
    def __init__(
            self,
            store_reference=None,
            factory=None,
            context=zmq.Context.instance(),
            event_handler=ClientEventHandler(),
            items_to_push=None,
            lock=None):
        # Threading
        threading.Thread.__init__(self)
        self.name = "Client_Network"
        self.daemon = False
        self._exit_event = threading.Event()
        self._event_handler = event_handler
        self._lock = lock

        # Replication
        self._factory = factory
        self._store_reference = store_reference
        self._id = "None"
        self._items_to_push = items_to_push

        # Networking
        self._context = context
        self._state = STATE_INITIAL
        self._config = {}

    def connect(self, id=None, address='127.0.0.1', port=5560):
        """
        Network socket setup
        """
        assert(id)

        if self._state == STATE_INITIAL:
            self._id = id 
            # uuid needed to avoid reconnexion problems on router sockets
            self._uuid = '{}_{}'.format(id,random_string_digits())
            logger.debug("connecting on {}:{}".format(address, port))
            self._command = self._context.socket(zmq.DEALER)
            self._command.setsockopt(zmq.IDENTITY, self._uuid.encode())
            self._command.connect("tcp://{}:{}".format(address, port))
            self._command.linger = 0

            self._subscriber = self._context.socket(zmq.DEALER)
            self._subscriber.setsockopt(zmq.IDENTITY, self._uuid.encode())
            self._subscriber.connect("tcp://{}:{}".format(address, port+1))
            self._subscriber.linger = 0

            self._publish = self._context.socket(zmq.PUSH)
            self._publish.setsockopt(zmq.IDENTITY, self._uuid.encode())
            self._publish.connect("tcp://{}:{}".format(address, port+2))
            self._publish.linger = 0

            # Run tll client
            self._ttl = ClientTTL(
                event_handler=self._event_handler,
                address=address,
                port=port+3,
                id=self._uuid)

            auth_request = RepAuthCommand(
                owner=self._id, data="AUTH_REQUEST:{}".format(self._id))
            auth_request.push(self._command)
            self._state = STATE_AUTH

            self.start()

    def _push_waiting_items(self):
        while not self._items_to_push.empty():
            node = self._items_to_push.get(False)

            if isinstance(node, (
                RepDeleteCommand, RepRightCommand,
                RepDisconnectCommand, RepUpdateUserMetadata)):
                node.push(self._command)
            else:
                node.push(self._publish)

    def request_snapshot(self):
        logger.info('Snapshots')
        snapshot_request = RepSnapshotCommand(
            owner=self._id,
            data="SNAPSHOT_REQUEST")

        snapshot_request.push(self._command)

        self._state = STATE_SYNCING

    def run(self):
        logger.debug("{} online".format(self._id))
        poller = zmq.Poller()
        poller.register(self._command, zmq.POLLIN)
        poller.register(self._subscriber, zmq.POLLIN)
        command_factory = ReplicatedCommandFactory()

        while not self._exit_event.is_set():
            items = dict(poller.poll(1))

            # DATA OUT
            if self._state == STATE_ACTIVE:
                self._push_waiting_items()

            # COMMANDS I/O
            if self._command in items:
                command = ReplicatedCommand.fetch(
                    self._command, command_factory)

                # AUTHENTIFICATION
                if isinstance(command, RepAuthCommand):
                    assert(self._state == STATE_AUTH)
                    if 'SUCCESS' in command.data:
                        self._state = STATE_CONFIG
                        config_init_request = RepConfigCommand(
                            owner=self._id, 
                            data="CONFIG_REQUEST")
                        config_init_request.push(self._command)
                        logger.info("Configuration")
                    if 'FAILURE' in command.data:
                        self._event_handler.on_server_lost()
                
                # DISCONNECT CONFIRMATION
                if isinstance(command, RepDisconnectCommand):
                    self._event_handler.on_disconnect_confirm()

                # CLIENTS INFO UPDATE
                if isinstance(command, RepUpdateClientsState):
                    if self._state == STATE_ACTIVE:
                        self._event_handler.on_clients_update(command.data)

                # CONFIGURATION
                if isinstance(command, RepConfigCommand):
                    assert(self._state == STATE_CONFIG)

                    self._config = command.data
                    self._event_handler.on_config_update(self._config)

                    if self._state == STATE_CONFIG:
                        self.request_snapshot()
                        self._snapshot_progress = 0
                        self._snapshot_total = 0

                # SNAPSHOT
                if isinstance(command, RepSnapshotCommand):
                    if 'length' in command.data:
                        self._snapshot_total = command.data.get('length')
                        if self._snapshot_total == 0:
                            self._state = STATE_ACTIVE
                            self._event_handler.on_connection()
                    if 'SNAPSHOT_END' in command.data:
                        logger.debug("END SNAPSHOT CLIENT")               

                # GRAPH OPERATION (DELETE, CHANGE_RIGHT)
                if type(command) in [RepDeleteCommand, RepRightCommand]:
                    command.execute(self._store_reference)
            
            # DATA IN
            if self._subscriber in items:
                datablock = ReplicatedDatablock.fetch(
                        self._subscriber, self._factory)
                datablock.store(self._store_reference)
                
                if self._state == STATE_SYNCING:
                    self._snapshot_progress+=1
                    if self._snapshot_progress == self._snapshot_total:
                        self._state = STATE_ACTIVE
                        self._event_handler.on_connection()
                   

        # Exit
        self._command.close()
        self._subscriber.close()
        self._publish.close()

        self._exit_event.clear()

    def stop(self):
        if self._ttl:
           self._ttl.stop()

        self._exit_event.set()
        self._state = 0


class ServerNetService(threading.Thread):
    def __init__(
            self,
            context=zmq.Context.instance(),
            config={
                'right_strategy': RP_STRICT,
            }):

        # Threading
        threading.Thread.__init__(self)
        self.name = "Server_Network"
        self.daemon = False
        self._exit_event = threading.Event()

        # Networking
        self._graph = ReplicationGraph()
        self._context = context
        self._command = None
        self._publisher = None
        self._pull = None
        self._state = 0
        self.clients = {}
        self._config = config
        self._ttl = None

    def listen(self, port=5560):
        self._port = port
        # Update request
        self._command = self._context.socket(zmq.ROUTER)
        self._command.setsockopt(zmq.IDENTITY, b'SERVER_COMMAND')
        self._command.bind("tcp://*:{}".format(port))
        self._command.linger = 0

        # Update all clients
        self._publisher = self._context.socket(zmq.ROUTER)
        self._publisher.setsockopt(zmq.IDENTITY, b'SERVER_PUSH')
        self._publisher.bind("tcp://*:{}".format(port+1))
        self._publisher.linger = 0

        # Update collector
        self._pull = self._context.socket(zmq.PULL)
        self._pull.setsockopt(zmq.IDENTITY, b'SERVER_PULL')
        self._pull.bind("tcp://*:{}".format(port+2))
        self._pull.linger = 0

        # TTL communication
        self._ttl_pipe = self._context.socket(zmq.DEALER)
        self._ttl_pipe.bind("inproc://ttl:{}".format(port+3))

        self._ttl = ServerTTL(
                    port=port+3
                )
    
        self.start()

    def disconnect_client(self, client):
        if client in self.clients:
            leaving_client = self.clients[client]
            
            cleanup_commands = []
            for key, node in self._graph.items():
                if node.owner == leaving_client:
                    cleanup_commands.append(
                        RepRightCommand(
                                owner='server',
                                data={
                                    'uuid': node.uuid,
                                    'owner': RP_COMMON
                                }

                        ))

            for del_cmd in cleanup_commands:
                for cli in self.clients.keys():
                    if cli != client and not self._command._closed :
                        self._command.send(cli, zmq.SNDMORE)
                        del_cmd.push(self._command)
                        del_cmd.execute(self._graph)
            
            del self.clients[client]
            logger.error("Disconnecting {}".format(leaving_client))
            logger.info(self.clients)

    def _clients_update(self):
        user_dict = {}
        for user, user_data in self.clients.items():
            user_dict[user.decode().split('_')[0]] = user_data 

        clients_states = RepUpdateClientsState(
            owner='server',
            data= user_dict
        )

        # Push it to every clients
        for cli_name in self.clients:
            self._command.send(cli_name, zmq.SNDMORE)
            clients_states.push(self._command)

    def _register_client(self, identity, id):
        for cli in self.clients.values():
            if id == cli['id']:
                logger.debug("client already added")
                return False
            
        
        logger.info("{} is connecting...".format(id))
        self.clients[identity] = {
            'id':id,
            'latency':999,
            'metadata':{},
        }
        
        return True

    def run(self):
        logger.debug("Server is online")
        poller = zmq.Poller()

        poller.register(self._command, zmq.POLLIN)
        poller.register(self._pull, zmq.POLLIN)
        poller.register(self._ttl_pipe, zmq.POLLIN)

        command_factory = ReplicatedCommandFactory()

        self._state = STATE_ACTIVE

        while not self._exit_event.is_set():
            # Non blocking poller
            socks = dict(poller.poll(1000))

            # COMMAND HANDLING
            if self._command in socks:
                command = ReplicatedCommand.server_fetch(
                    self._command, command_factory)
                    
                # CLIENT DISCONNECTION (wip
                if isinstance(command, RepDisconnectCommand):
                    self._ttl_pipe.send_multipart([b'STOP_WATCHING',command.sender])
                    self.disconnect_client(command.sender)
                    confirmation = RepDisconnectCommand(
                        owner='server'
                    )
                    self._command.send(command.sender, zmq.SNDMORE)
                    confirmation.push(self._command)

                # AUHTENTIFICATION
                if isinstance(command, RepAuthCommand):
                    auth_data = command.data.split(':')
                    if 'AUTH_REQUEST' in auth_data[0]:                      
                        success = self._register_client(command.sender,auth_data[1])
                        
                        auth_status = 'SUCCESS' if success else 'FAILURE'
                        
                        auth_response = RepAuthCommand(
                            owner="server",
                            data=auth_status)

                        self._command.send(command.sender, zmq.SNDMORE)
                        auth_response.push(self._command)

                # CONFIGURATION
                if isinstance(command, RepConfigCommand):
                    config = copy.copy(self._config)
                    # config['ttl_port'] = self.clients[command.sender] 

                    config_frame = RepConfigCommand(
                        owner="server",
                        data=config)

                    self._command.send(command.sender, zmq.SNDMORE)
                    config_frame.push(self._command)

                # SNAPSHOT
                if isinstance(command, RepSnapshotCommand):
                    # Sending snapshots
                    keys_to_push = []

                    for key, item in self._graph.items():
                        deps = []
                        self._graph.get_dependencies(key, deps=deps)

                        for dep in list(reversed(deps)):
                            if dep not in keys_to_push:
                                keys_to_push.append(dep)

                        if key not in keys_to_push:
                            keys_to_push.append(key)
                    
                    snapshot_state = RepSnapshotCommand(
                        owner='server', data={'length':len(keys_to_push)})
                    self._command.send(command.sender, zmq.SNDMORE)
                    snapshot_state.push(self._command)

                    for keys in keys_to_push:
                        node_to_send = self._graph.get(keys)
                        if node_to_send:
                            self._publisher.send(command.sender, zmq.SNDMORE)
                            node_to_send.push(self._publisher)

                    # Snapshot end
                    snapshot_end = RepSnapshotCommand(
                        owner='server', data='SNAPSHOT_END')
                    self._command.send(command.sender, zmq.SNDMORE)
                    snapshot_end.push(self._command)

                # CLIENT METADATA
                if isinstance(command, RepUpdateUserMetadata):
                    user = self.clients.get(command.sender)
                    
                    if user :
                        user['metadata'].update(command.data)
                        self._clients_update()

                # OTHERS
                if type(command) in [RepDeleteCommand, RepRightCommand]:
                    command.execute(self._graph)
                    for cli_name in self.clients:
                        if cli_name != command.sender:
                            self._command.send(cli_name, zmq.SNDMORE)
                            command.push(self._command)

            # TTL HANDLING
            if self._ttl_pipe in socks:
                notification = self._ttl_pipe.recv_multipart()

                if notification[0] == b'STATE':
                    clients_states = msgpack.unpackb(notification[1],raw=False)

                    # Prevent client state update on non logged ones
                    # TODO: start ttl properly
                    if len(clients_states) != len(self.clients):
                        continue
                    # Prepare update
                    for id,state in clients_states.items():
                        self.clients[id.encode()]['latency'] = state['latency'] 

                    self._clients_update()
                
                if notification[0] == b'LOST':
                    self.disconnect_client(notification[1])
            # Regular update  routing (Clients / Server / Clients)
            if self._pull in socks:
                datablock = ServerReplicatedDatablock.fetch(self._pull)
                datablock.store(self._graph)

                # Update all clients
                for cli_name in self.clients.keys():
                    if cli_name != datablock.sender:
                        self._publisher.send(cli_name, zmq.SNDMORE)
                        datablock.push(self._publisher)

        self._command.close()
        self._pull.close()
        self._publisher.close()
        self._ttl_pipe.close()

        self._exit_event.clear()

    def stop(self):
        if self._ttl:
            self._ttl.stop()

        self._exit_event.set()
        self._state = 0
        self.join()   


class ClientTTL(threading.Thread):
    def __init__(
            self,
            context=zmq.Context.instance(),
            event_handler=ClientEventHandler(),
            address="127.0.0.1",
            id=None,
            port=5562,
            timeout=1000):
        # Threading
        threading.Thread.__init__(self)
        self.name = "Client_TTL"
        self.daemon = False
        self._id = id
        self._event_handler = event_handler
        self._exit_event = threading.Event()

        # Networking
        self._context = context
        
        self._command = self._context.socket(zmq.DEALER)
        self._command.setsockopt(zmq.IDENTITY, self._id.encode())
        self._command.connect("tcp://{}:{}".format(address, port))
        self._command.linger = 0
        self._timeout = timeout

        self.start()

    def run(self):
        poller = zmq.Poller()
        poller.register(self._command, zmq.POLLIN)

        self._command.send(b"INIT")

        while not self._exit_event.is_set():    
            socks = dict(poller.poll(self._timeout))

            if self._command in socks:
                self._command.recv(0)
                time.sleep(0.005)
                self._command.send(b"PONG")
            else:
                self._event_handler.on_server_lost()
 
        self._command.close()

    def stop(self):
        self._exit_event.set()


class ServerTTL(threading.Thread):
    def __init__(
            self,
            context=zmq.Context.instance(),
            event_handler=None,
            port=5562,
            timeout=1000):
        # Threading
        threading.Thread.__init__(self)
        self.name = "server_TTL"
        self.daemon = False
        self._id = id
        self._event_handler = event_handler
        self._exit_event = threading.Event()

        # Networking
        self._context = context
        self._command = self._context.socket(zmq.ROUTER)
        self._command.bind("tcp://*:{}".format(port))
        self._command.linger = 0
        self._pipe = self._context.socket(zmq.DEALER)
        self._pipe.connect("inproc://ttl:{}".format(port))

        self._timeout = timeout
        self._state = STATE_INITIAL
        self._clients_state = {}

        self.start()

    def run(self):
        self._state = STATE_ACTIVE
        poller = zmq.Poller()

        poller.register(self._command, zmq.POLLIN)
        poller.register(self._pipe, zmq.POLLIN)
        last_update_time = current_milli_time()
        while not self._exit_event.is_set():
            socks = dict(poller.poll(1))
            current_time = current_milli_time()

            if self._command in socks:
                identity, frame = self._command.recv_multipart(0)
                
                username = identity.decode()
                
                
                if frame == b'INIT':
                    self._clients_state[username] = {}
                    self._clients_state[username]['latency'] = 999
                    self._clients_state[username]['last_update'] = current_time
                
                client =  self._clients_state.get(username)

                if client is None:
                    continue

                self._command.send(identity, zmq.SNDMORE)
                self._command.send(b"PING")

                # Update delay information
                client['latency'] =  (current_time-(client['last_update']))-5
                client['last_update'] = current_time

            if self._pipe in socks:
                notification = self._pipe.recv_multipart()

                if notification[0] == b'STOP_WATCHING':
                    self.stop_monitor(notification[1].decode())

            if current_time-last_update_time > 1000:
                last_update_time = current_time
                client_to_remove = []

                # Check clients status
                for client in self._clients_state.keys():
                    ping =  (current_time-(self._clients_state[client]['last_update']))

                    if ping > self._timeout:
                        logger.error("Lost connexion to {}".format(client))
                        client_to_remove.append(client)
                        self._pipe.send_multipart([b'LOST',client.encode()])

                for client in client_to_remove:
                    self.stop_monitor(client)
                
                self._pipe.send_multipart([b'STATE',msgpack.packb(self._clients_state,use_bin_type=True)])
         
        self._command.close()
        self._pipe.close()
        self._state = STATE_INITIAL
    
    def stop_monitor(self, client):
        if client in self._clients_state:
            logger.info("removing {}".format(client))
            del self._clients_state[client]

    @property
    def state(self):
        return self._state

    @property
    def clients_state(self):
        return copy.copy(self._clients_state)

    def stop(self):
        self._exit_event.set()
