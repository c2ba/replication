from setuptools import setup

setup(name='replication',
      version='0.1',
      description='A prototype object replication lib',
      url='https://gitlab.com/slumber/replication',
      author='Swann Martinez',
      author_email='swann.martinez@pm.me',
      license='GPL3',
      packages=['replication'],
      install_requires=[
          'zmq',
          'msgpack',
          'jsondiff',
      ],
      test_suite='tests',
      tests_require=['nose'],

      zip_safe=False)