import msgpack
import copy
from replication.data import ReplicatedDatablock, ReplicatedDataFactory
from replication.graph import ReplicationGraph

class SampleData():
    def __init__(self, map={"sample": bytearray(50000)}):
        self.map = map


class RepSampleData(ReplicatedDatablock):
    def dump(self,pointer=None):
        return copy.copy(pointer.map)

    def load(self,data=None, target=None):
        target.map["sample"] = data["sample"]

    def construct(self, data):
        return SampleData()


def get_sample_factory():
    """Generate a factory with the sample data type registered
    """
    factory = ReplicatedDataFactory()
    factory.register_type(SampleData, RepSampleData)

    return factory

    
def generate_graph():
    """Util function that generate a test graph:

                A - B - D
                 \
                  C
    """
    graph = ReplicationGraph()
    factory = ReplicatedDataFactory()
    factory.register_type(SampleData, RepSampleData)

    a = SampleData()
    b = SampleData()
    c = SampleData()
    d = SampleData()

    rd = factory.get_implementation_from_object(d)(owner="d", pointer=d)
    rb = factory.get_implementation_from_object(b)(owner="b", pointer=b,dependencies=[rd.uuid])
    rc = factory.get_implementation_from_object(c)(owner="c", pointer=c)
    ra = factory.get_implementation_from_object(a)(owner="a", pointer=a,dependencies=[rb.uuid,rc.uuid])
    
    rd.store(graph)
    rb.store(graph)
    rc.store(graph)
    ra.store(graph)
    
    return graph