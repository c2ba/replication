import cProfile
import logging
import re
import time
import unittest

from replication.constants import *
from replication.interface import Server, Session
from tests.test_utils import *

logger = logging.getLogger(__name__)


class TestInterface(unittest.TestCase):
    def __init__(self, methodName='runTest'):
        unittest.TestCase.__init__(self, methodName)

    def test_empty_snapshot(self):
        # Setup
        client = Session(factory=get_sample_factory())

        client.host(port=5570, id="client_test_callback")
        
        time.sleep(0.2)
        test_state = client.state
        client.disconnect()

        self.assertEqual(test_state, STATE_ACTIVE)

    def test_filled_snapshot(self):
        # Setup
        client = Session(factory=get_sample_factory())
        client2 = Session(factory=get_sample_factory())

        client.host(port=5575, id="cli_test_filled_snapshot")

        # Test the key registering
        data_sample_key = client.add(SampleData())
        client.commit(data_sample_key)
        client.push_all()

        client2.connect(port=5575, id="client_2")
        time.sleep(0.2)
        rep_test_key = client2.get(uuid=data_sample_key)

        client.disconnect()
        client2.disconnect()

        self.assertEqual(data_sample_key, rep_test_key.uuid)

    def test_client_add(self):
        # Setup environment
        client = Session(factory=get_sample_factory())
        client.host(port=5580, id="cli_test_add_client_data")
        time.sleep(0.2)
        # Test the key adding
        data_sample_key = client.add(SampleData())

        test_state = client._graph[data_sample_key].state
        client.disconnect()

        self.assertEqual(test_state, ADDED)

    def test_client_push(self):
        # Setup environment
        client = Session(factory=get_sample_factory())
        client.host(port=5565, id="cli_test_add_client_data")

        client2 = Session(factory=get_sample_factory())
        client2.connect(port=5565, id="cli2_test_add_client_data")

        # Test the key adding
        data_sample_key = client.add(SampleData())
        client.commit(data_sample_key)
        client.push_all()

        time.sleep(0.3)
        data_sample_state = client._graph[data_sample_key].state


        # Waiting for server to receive the datas
        rep_test_key = client2.get(uuid=data_sample_key)

        client.disconnect()
        client2.disconnect()

        succed = rep_test_key.uuid == data_sample_key and data_sample_state == UP

        self.assertTrue(succed)
    
    def test_client_push_dependencies(self):
        # Setup environment
        client = Session(factory=get_sample_factory())
        client.host(port=5600, id="cli_test_push_client_data")

        c = client.add(SampleData())
        d = client.add(SampleData())
        b = client.add(SampleData(),dependencies=[d])
        a = client.add(SampleData(),dependencies=[b,c])
        client.commit(c)
        client.commit(d)
        client.commit(b)
        client.commit(a)
        client.push_all()

        client2 = Session(factory=get_sample_factory())
        client2.connect(port=5600, id="cli2_test_push_client_data")

        time.sleep(0.2)
        test_deps = len(client2.list())

        client.disconnect()
        client2.disconnect()

        self.assertTrue(test_deps == 4)

    def test_client_apply(self):
        # Setup environment
        client = Session(factory=get_sample_factory())
        client.host(port=5585, id="cli_test_client_data_intergity")

        client2 = Session(factory=get_sample_factory())
        client2.connect(port=5585, id="cli2_test_client_data_intergity")

        # Test the key registering
        data_sample_key = client.add(SampleData())
        client.commit(data_sample_key)
        client.push_all()
        initial = client.get(uuid=data_sample_key)

        # Waiting for server to receive the datas
        time.sleep(2)

        client2.apply_all()
        result = client2.get(uuid=data_sample_key)

        client.disconnect()
        client2.disconnect()

        self.assertEqual(result.pointer.map["sample"], initial.pointer.map["sample"])

    def test_client_remove(self):
        client = Session(factory=get_sample_factory())
        client.host(port=5590, id="cli_test_client_data_intergity")

        client2 = Session(factory=get_sample_factory())
        client2.connect(port=5590, id="cli2_test_client_data_intergity")

        # Test the key registering
        data_sample_key = client.add(SampleData())
        client.push_all()

        test_map_result = SampleData()

        # Waiting for server to receive the datas
        time.sleep(.1)

        client2.apply_all()

        client.remove(data_sample_key)
        time.sleep(.1)

        client.disconnect()
        client2.disconnect()

        self.assertFalse(data_sample_key in client._graph)

    def test_client_diff(self):
        factory = ReplicatedDataFactory()
        factory.register_type(SampleData, RepSampleData, timer=.1, automatic=False)

        client = Session(factory=factory)
        client.host(port=5710, id="cli_test_client_data_intergity")
        time.sleep(1)

        sample = SampleData()
        
        # Test the key registering
        data_sample_key = client.add(sample)
        client.commit(data_sample_key)
        client.push(data_sample_key)
        sample.map["sample"] = "asdasdasd"
        time.sleep(1)
        test_state = client.get(uuid=data_sample_key).state
        client.disconnect()
        self.assertEqual(test_state, MODIFIED)

    def test_client_automatic_push(self):
        factory = ReplicatedDataFactory()
        factory.register_type(SampleData, RepSampleData, timer=.1, automatic=True)

        client = Session(factory=factory)
        client.host(port=5720, id="cli_test_client_data_intergity")
        time.sleep(1)
        sample = SampleData()
        
        # Test the key registering
        data_sample_key = client.add(sample)
        client.commit(data_sample_key)
        client.push_all()

        sample.map["sa"] = bytearray(500)
        #Wait for a check
        time.sleep(1)
        test_state = client.get(uuid=data_sample_key).state
        client.disconnect()
        self.assertEqual(test_state, UP)


class TestStressTestInterface(unittest.TestCase):
    def test_stress_register(self):
        total_time = 0
     # Setup
        client = Session(factory=get_sample_factory())
        client2 = Session(factory=get_sample_factory())

        client.host(port=5595, id="cli_test_filled_snapshot")
        client2.connect(port=5595, id="client_2")

        # Test the key registering
        for i in range(100):
            key = client.add(SampleData())
            client.commit(key)

        client.push_all()

        while len(client2._graph.keys()) < 100:
            time.sleep(0.00001)
            total_time += 0.00001

        client.disconnect()
        client2.disconnect()
        logger.debug("{} s for 10000 values".format(total_time))

        self.assertLess(total_time, 1)
