import cProfile
import logging
import re
import time
import unittest

logging.basicConfig()
logger = logging.getLogger(__name__)


from tests.test_utils import *
from replication.graph import ReplicationGraph



class TestGraph(unittest.TestCase):
    
    def test_graph_list(self):
        graph = generate_graph()

        test_list = graph.list()

        # logger.info(test_list)
        self.assertTrue(isinstance(test_list,list))
    
    def test_graph_get_dependencies(self):
        graph = generate_graph()
        # logger.info(graph.list())
        path = []
        
        graph.get_dependencies(list(graph.keys())[3],path)

        self.assertTrue(len(path)==3)


def suite():
    suite = unittest.TestSuite()

    # Data factory
    suite.addTest(TestGraph('test_graph_list'))


    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite())
