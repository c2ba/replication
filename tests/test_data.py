import cProfile
import logging
import re
import time
import unittest

logger = logging.getLogger(__name__)


from tests.test_utils import *
from replication.interface import Session, Server


class TestDataFactory(unittest.TestCase):
    def test_data_factory(self):
        factory = ReplicatedDataFactory()
        factory.register_type(SampleData, RepSampleData)
        data_sample = SampleData()
        rep_sample = factory.get_implementation_from_object(
            data_sample)(owner="toto", pointer=data_sample)

        self.assertEqual(isinstance(rep_sample, RepSampleData), True)


def suite():
    suite = unittest.TestSuite()

    # Data factory
    suite.addTest(TestDataFactory('test_data_factory'))


    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite())
    